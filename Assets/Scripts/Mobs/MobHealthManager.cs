﻿using Berserk.Items;
using Berserk.Player;
using System.Collections;
using UnityEngine;


namespace Berserk.Mobs
{
    public class MobHealthManager : MonoBehaviour
    {
        #region Fields
        [Header("Sounds")]
        [SerializeField] private AudioClip _damageSound;
        [Header("Stats")]
        [SerializeField] private float _health = 20.0f;
        private GameObject _player;
        private float _currentHealth;
        private Coroutine _damageCoroutine;
        #endregion


        #region UnityMethods
        void Start()
        {
            _currentHealth = _health;
            _player = transform.parent.gameObject.GetComponent<MobSpawner>().Player;
        }

        void Update()
        {
            CheckHealth();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Weapon"))
            {
                if (_player.GetComponent<Animator>().GetBool("Attack"))
                {
                    float damagePoints = other.gameObject.GetComponent<WeaponController>().Damage;
                    _damageCoroutine = StartCoroutine(ToDamage(damagePoints));
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.CompareTag("Weapon") && _damageCoroutine != null)
                StopCoroutine(_damageCoroutine);
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Knife"))
            {
                Adjust(collision.gameObject.GetComponent<KnifeController>().Damage);
            }
        }
        #endregion


        #region Methods
        private void CheckHealth()
        {
            if (_currentHealth > _health) _currentHealth = _health;
            else if (_currentHealth < 0)
            {
                _player.GetComponent<PlayerScoreCounter>().KillEnemy();
                Die(2.0f);
            }
        }

        public void Adjust(float point)
        {
            _currentHealth -= point;
        }

        private void Die(float timeForAnimation)
        {
            GetComponent<Animator>().SetBool("isDead", true);
            if (timeForAnimation > 0)
            {
                timeForAnimation -= Time.deltaTime;
                Die(timeForAnimation);
            }
            else Destroy(gameObject);
        }

        private IEnumerator ToDamage(float point)
        {
            while (true)
            {
                AudioSource.PlayClipAtPoint(_damageSound, transform.position);
                Adjust(point);
                yield return new WaitForSeconds(1);
            }
        }

        #endregion
    }
}
