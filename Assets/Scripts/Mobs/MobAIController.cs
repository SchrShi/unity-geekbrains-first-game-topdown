﻿using Berserk.Items;
using Berserk.Player;
using System.Collections;
using UnityEngine;


namespace Berserk.Mobs
{
    public class MobAIController : MonoBehaviour
    {
        #region Fields
        [SerializeField] private float _distanceToPlayer = 20.0f;
        [SerializeField] private float _walkSpeed = 2.0f;
        [SerializeField] private float _damage;

        private Animator _animator;
        private Rigidbody _rigidbody;
        private Vector3 _startPoint;
        private GameObject _player;
        private Coroutine _playerDamageCoroutine;
        private float _distanceToPlayerForAttack = 1.0f;
        private bool _isWalkToSpawn = false;

        #endregion


        #region UnityMethods
        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _animator = GetComponent<Animator>();
            _player = transform.parent.gameObject.GetComponent<MobSpawner>().Player;
            _startPoint = transform.position;
        }

        private void FixedUpdate()
        {
            CheckDistance();
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Player"))
                _playerDamageCoroutine = StartCoroutine(collision.gameObject.GetComponent<PlayerHealthManager>().Damage(_damage));
        }

        private void OnCollisionExit(Collision collision)
        {
            if (collision.gameObject.CompareTag("Player") && _playerDamageCoroutine != null)
                StopCoroutine(_playerDamageCoroutine);
        }
        #endregion


        #region Methods

        private void CheckDistance()
        {
            RaycastHit hit;
            Vector3 distance = _player.transform.position - transform.position;
            float DistanceForAttack = _distanceToPlayerForAttack * _distanceToPlayerForAttack;
            var raycast = Physics.Raycast(transform.position, _player.transform.position - transform.position, out hit, _distanceToPlayer);

            if (distance.sqrMagnitude <= DistanceForAttack)
            {
                Attack();
            }

            else if (raycast)
            {
                Debug.DrawRay(transform.position, _player.transform.position - transform.position, 
                    Color.red, _distanceToPlayer);
                if (hit.collider.gameObject.CompareTag("Player"))
                {
                    _isWalkToSpawn = false;
                    Movement(_player.transform.position);
                }
                else
                {
                    if (!_isWalkToSpawn)
                    {
                        StartCoroutine(Wait());
                    }

                    else if (_isWalkToSpawn)
                    {
                        GoToStart();
                    }
                }
            }
        }

        private void Movement(Vector3 target)
        {
                var direction = (target - transform.position).normalized;
                _animator.SetBool("isAttack", false);
                _animator.SetBool("isWalk", true);
                transform.rotation = Quaternion.LookRotation(direction);
                _rigidbody.velocity = direction * _walkSpeed;
        }

        private void Attack()
        {
            {
                var direction = (_player.transform.position - transform.position).normalized;
                _animator.SetBool("isWalk", false);
                transform.rotation = Quaternion.LookRotation(direction);
                _animator.SetTrigger("Attack");
            }
        }

        private void GoToStart()
        {
            if ((_startPoint - transform.position).sqrMagnitude > 1.0f && _isWalkToSpawn)
            {
                Movement(_startPoint);
            }
            else
            {
                _animator.SetBool("isWalk", false);
            }
        }

        private IEnumerator Wait()
        {
             _animator.SetBool("isWalk", false);
             yield return new WaitForSeconds(5);
            _isWalkToSpawn = true;
        }
        #endregion
    }
}
