﻿using UnityEngine;


namespace Berserk.Mobs
{
    public class MobSpawner : MonoBehaviour
    {
        #region Fields

        [SerializeField]private GameObject _player;
        [SerializeField] private GameObject _mob;
        [SerializeField] private float _distanceToPlayer = 15.0f;

        private bool _isSpawn = false;

        #endregion


        #region Properties

        public GameObject Player
        {
            get
            {
                return _player;
            }
        }

        #endregion


        #region UnityMethods

        private void Update()
        {
                Vector3 distance = Player.transform.position - transform.position;
                if (distance.sqrMagnitude <= (_distanceToPlayer * _distanceToPlayer) && !_isSpawn)
                {
                    var monster = Instantiate(_mob, new Vector3(transform.position.x, transform.position.y, transform.position.z), new Quaternion());
                    monster.transform.parent = gameObject.transform;
                    _isSpawn = true;
                }
        }

        #endregion
    }
}
