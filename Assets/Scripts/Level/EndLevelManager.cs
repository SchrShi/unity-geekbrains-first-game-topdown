﻿using UnityEngine;


namespace Berserk.Level
{
    public class EndLevelManager : MonoBehaviour
    {
        #region Fields
        [SerializeField] private EndGameManager _endGame;
        #endregion

        #region UnityMethods
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                _endGame.EndGame(true);
            }
        }
        #endregion
    }
}
