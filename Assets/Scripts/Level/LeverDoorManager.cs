﻿using UnityEngine;


namespace Berserk
{
    public class LeverDoorManager : MonoBehaviour
    {
        #region Fields

        [SerializeField] private AudioClip _openSound;
        [SerializeField] private GameObject[] _levers;

        [HideInInspector] public int leversEnabledCount;
        private float _soundVolume = 450.0f;
        private bool _isOpen = false;

        #endregion


        #region UnityMethods
        private void Awake()
        {
            foreach (var lever in _levers) lever.transform.parent = gameObject.transform;
        }

        void Update()
        {
            if (leversEnabledCount == _levers.Length && !_isOpen)
            {
                OpenDoor();
            }
        }

        private void OpenDoor()
        {
            print("Дверь открыта");
            AudioSource.PlayClipAtPoint(_openSound, transform.position, _soundVolume);
            GetComponent<Animator>().SetTrigger("Open");
            _isOpen = true;
        }

        #endregion
    }
}
