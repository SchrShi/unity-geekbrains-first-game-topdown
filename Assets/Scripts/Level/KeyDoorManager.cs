﻿using Berserk.Player;
using UnityEngine;


namespace Berserk
{
    public class KeyDoorManager : MonoBehaviour
    {
        #region Fields

        [SerializeField] private AudioClip _openSound;
        [SerializeField] private PlayerScoreCounter _scoreCounter;

        #endregion


        #region UnityMethods

        private void Start()
        {
        }

        private void Update()
        {
            var keyCount = _scoreCounter.KeysCount;
            var sqrDistance = (_scoreCounter.gameObject.transform.position - transform.position).sqrMagnitude;

            if (sqrDistance < 9)
            {
                if (keyCount == 4 &&
                    Input.GetKeyDown(KeyCode.E))
                {
                    GetComponent<Animator>().SetTrigger("Open");
                    AudioSource.PlayClipAtPoint(_openSound, transform.position);
                }
                else print("Недостаточно ключей");
            }
        }

        #endregion
    }
}
