﻿using UnityEngine;


namespace Berserk
{
    public class LeverManager : MonoBehaviour
    {
        #region Fields
        [SerializeField] private GameObject _player;
        [SerializeField] private AudioClip _soundTouch;
        [SerializeField] private float _pauseTime = 1.0f;

        private LeverDoorManager _door;
        private bool _isChecked = false;
        private bool _hasChecked = true;
        private float _pause;
        #endregion


        #region UnityMethods
        private void Start()
        {
            _pause = _pauseTime;
            _door = gameObject.transform.parent.gameObject.GetComponent<LeverDoorManager>();
        }

        private void Update()
        {
            CoolCheck();
            var distantionSqr = (_player.transform.position - transform.position).sqrMagnitude;
            if (distantionSqr <= 9.0f && Input.GetKeyDown(KeyCode.E)) 
            {
                print("Игрок возле рычага");
                if (!_isChecked)
                {
                    _door.leversEnabledCount += 1;
                    print("Рычаг " + _door.leversEnabledCount + " нажат");
                    GetComponent<Animator>().SetTrigger("Touch");
                    AudioSource.PlayClipAtPoint(_soundTouch, transform.position);
                }
                else if (_isChecked)
                {
                    _door.leversEnabledCount -= 1;
                    print("Рычаг " + _door.leversEnabledCount + " отключен");
                    GetComponent<Animator>().SetTrigger("TouchOff");
                    AudioSource.PlayClipAtPoint(_soundTouch, transform.position);
                }
                _isChecked = !_isChecked;
                _hasChecked = false;
            }
        }

        private void CoolCheck()
        {
            if (!_hasChecked)
            {
                _pause -= _pauseTime;
            }

            if(_pause <= 0)
            {
                _hasChecked = !_hasChecked;
            }
        }

        #endregion
    }
}
