﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGameManager : MonoBehaviour
{
    [SerializeField] private GameObject _spriteOnLose;
    [SerializeField] private GameObject _spriteOnWin;

    private static float _pause = 3.0f;
    private static bool isEnd = false;

    private void Update()
    {
        if(isEnd)
        _pause -= Time.deltaTime;
    }

    public void EndGame(bool isWin)
    {
        isEnd = true;
        if (!isWin) _spriteOnLose.SetActive(true);
        if (isWin) _spriteOnWin.SetActive(true);
        if (_pause<=0) SceneManager.LoadScene("SampleScene");

    }
}
