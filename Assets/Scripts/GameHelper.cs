﻿using TMPro;
using UnityEngine;


namespace Berserk 
{
    public class GameHelper : MonoBehaviour
    {
        #region Fields

        [SerializeField] private TextMeshProUGUI _learnText;
        private string[] _helps;
        private int _completed = 0;

        #endregion


        #region UnityMethods
        private void Start()
        {
            _helps = new string[]
            {
            "W/стрелка вверх и S/стрелка вниз - движение вперед или назад\nA/стрелка влево и D/стрелка вправо - поворот влево/вправо\nSPACE - прыжок",
            "TAB - таргетирование на ближайшем противнике",
            "LCTRL - удар мечом, LSHIFT - бросить бомбу, LALT - бросить нож",
            "Спасибо, что прошли небольшой туториал по управлению. \nПриятной игры!"
            };
        }

        private void Update()
        {
            _learnText.SetText(_helps[_completed]);
            if (_completed == 0 && (Input.GetButtonDown("Vertical") || Input.GetButtonDown("Horizontal"))) _completed++;
            else if (_completed == 1 && Input.GetKeyDown(KeyCode.Tab)) _completed++;
            else if (_completed == 2 && (Input.GetButtonDown("Fire1") || Input.GetButtonDown("Fire2") || Input.GetButtonDown("Fire3"))) _completed++;
        }

        #endregion
    }
}