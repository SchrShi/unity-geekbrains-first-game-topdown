﻿using Berserk.Mobs;
using UnityEngine;


namespace Berserk.Gun
{
    
    public class TNTController : MonoBehaviour
    {
        #region Fields
        [SerializeField] private float _radius;
        [SerializeField] private float _damage;
        [SerializeField] private float _force;
        #endregion


        #region UnityMethods
        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Mob"))
            {
                ExplosionsWithoutAddExplosionForce();
                //ExplosionWithAddForce();
                Destroy(gameObject);
            }
        }
        #endregion


        #region Methods
        private void ExplosionWithAddExlosionForce()
        {
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, _radius);
            foreach(var hit in hitColliders)
            {
                if (hit.gameObject.CompareTag("Mob"))
                {
                    hit.gameObject.GetComponent<Rigidbody>().AddExplosionForce(_force, transform.position, _radius);
                    hit.gameObject.GetComponent<MobHealthManager>().Adjust(_damage);
                }
            }
        }

        private void ExplosionsWithoutAddExplosionForce()
        {
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, _radius);
            foreach(var hit in hitColliders)
            {
                if (hit.gameObject.CompareTag("Mob"))
                {
                    var distance = hit.gameObject.transform.position - transform.position;
                    hit.gameObject.GetComponent<Rigidbody>().AddForce(distance.normalized * _force);
                    hit.gameObject.GetComponent<MobHealthManager>().Adjust(_damage);
                }
            }
        }
        #endregion
    }
}
