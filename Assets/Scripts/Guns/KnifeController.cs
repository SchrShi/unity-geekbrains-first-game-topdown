﻿using Berserk.Items;
using UnityEngine;

public class KnifeController : Items
{
    #region Fields

    [SerializeField] private float _damage;
    [SerializeField] private float _throwForce;

    private Rigidbody _rigidbody;
    
    #endregion


    #region Properties
    public float Damage
    {
        get
        {
            return _damage;
        }
        private set
        {
            _damage = value;
        }
    }
    #endregion


    #region UnityMethods
    private void Start()
    {
            _rigidbody = GetComponent<Rigidbody>();
            _rigidbody.AddForce(transform.forward * _throwForce, ForceMode.Impulse);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Physics.IgnoreCollision(gameObject.GetComponent<Collider>(), collision.gameObject.GetComponent<Collider>());
        }
    }
    #endregion
}
