﻿using UnityEngine;



namespace Berserk
{
    public class CameraController : MonoBehaviour
    {
        #region Fields

        [SerializeField] private Transform _player;

        [Header("Without stance")]
        [SerializeField] private float _x;
        [SerializeField] private float _y;
        [SerializeField] private float _z;

        [Header("With stance")]
        [SerializeField] private float _stanceX;
        [SerializeField] private float _stanceY;
        [SerializeField] private float _stanceZ;

        private Animator _playerAnimator;

        #endregion


        #region UnityMethods
        private void Start()
        {
            _playerAnimator = _player.gameObject.GetComponent<Animator>();
        }

        private void Update()
        {
            if (!_playerAnimator.GetBool("Stance"))
            {
                if (transform.parent != null) transform.parent = null;
                transform.position = new Vector3(_player.position.x + _x, _player.position.y + _y, _player.position.z + _z);
                transform.LookAt(_player);
            }
            else
            {
                transform.parent = _player;
                transform.rotation = _player.rotation;
                transform.localPosition = new Vector3(_stanceX, _stanceY, _stanceZ);
            }
        }

        #endregion
    }
}
