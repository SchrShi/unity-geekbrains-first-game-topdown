﻿using Berserk.Mobs;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;


namespace Berserk.Player
{
    public class PlayerHealthManager : MonoBehaviour
    {
        #region Fields
        [SerializeField] private float _playerHealth = 100.0f;
        [SerializeField] private Slider _slider;
        [SerializeField] private EndGameManager _endGame;

        private float _currentHealth;
        #endregion

        #region UnityMethods
        private void Start()
        {
            _currentHealth = _playerHealth;
        }

        private void Update()
        {
            if (_currentHealth > _playerHealth) _currentHealth = _playerHealth;

            else if (_currentHealth <= 0)
            {
                _endGame.EndGame(false);
            }
            _slider.value = _currentHealth;
        }
        #endregion

        #region Methods
        public IEnumerator Damage(float point)
        {
            while (true)
            {
                _currentHealth -= point;
                yield return new WaitForSeconds(1);
            }
        }

        public void Health(float point)
        {
            _currentHealth += point;
        }
        #endregion
    }
}
