﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace Berserk.Player
{
    public class PlayerController : MonoBehaviour
    {
        #region Fields
        [Header("Guns Object")]
        [SerializeField] private GameObject _knife;
        [SerializeField] private GameObject _bomb;
        [SerializeField] private Transform _gun;
        [Header("Player Stats")]
        [SerializeField] private float _jumpForce = 10000.0f;
        [SerializeField] private float _turnSpeed = 5.0f;
        [SerializeField] private float _walkSpeed = 5.0f;
        [Header("Other")]
        [SerializeField] private Camera _mainCamera;

        private Rigidbody _rigidbody;
        private Animator _animator;

        private float _vertical;
        private float _horizontal;
        private bool _hasPlane = true;

        private float _coolDown = 1.0f;
        private bool _isKnifeCoolDown = true;
        private bool _isBombCoolDown = true;

        private float _rayLegnth;

        #endregion


        #region UnityMethods

        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _animator = GetComponent<Animator>();
        }

        private void Update()
        {
            _vertical = Input.GetAxisRaw("Vertical");
            _horizontal = Input.GetAxisRaw("Horizontal");

            Jump();
            StandToAttack(!_animator.GetBool("Stance"));
            Turn();
            Attack();
            ThrowAttack();
            ThrowBomb();
        }

        private void FixedUpdate()
        {
            Movement();
        }

        #endregion


        #region Methods

        private void Movement()
        {
            Vector3 _movement;
            if (_hasPlane) _animator.SetInteger("VerticalMovement", (int)_vertical);
            if (!_animator.GetBool("Stance"))
            {
                _movement = new Vector3(transform.forward.x * _vertical * _walkSpeed, _rigidbody.velocity.y, transform.forward.z * _vertical * _walkSpeed);
                
            }
            else
            {
                _animator.SetInteger("HorizontalMovement", (int)_horizontal);
                _movement = new Vector3(transform.forward.x * _horizontal * _walkSpeed, transform.forward.y, transform.forward.z * _vertical * _walkSpeed);

            }
            _rigidbody.velocity = _movement;

        }

        private void Turn()
        {
            if (!_animator.GetBool("Stance"))
            {
                _animator.SetInteger("HorizontalMovement", (int)_horizontal);
                transform.Rotate(Vector3.up * _horizontal * _turnSpeed, Space.Self);
            }
            else 
            {
                var target = Targeting(GameObject.FindGameObjectsWithTag("Mob"));
                transform.LookAt(target);
            }
        }

        private void Jump()
        {
            if (_hasPlane)
            {
                if (Input.GetButtonDown("Jump"))
                {
                    ToJump(_jumpForce);
                }
                else if (Input.GetButton("Jump"))
                {
                    ToJump(_jumpForce + 2.0f);
                }
            }
        }

        private void ToJump(float force)
        {
            print(force);
            _animator.SetTrigger("Jump");
            _rigidbody.AddForce(new Vector3(0, force, 0), ForceMode.Impulse);
            _hasPlane = false;
            JumpCoolDown(0.5f);
        }

        private void Attack()
        {
            if (Input.GetButton("Fire1"))
                _animator.SetBool("Attack", true);
            else _animator.SetBool("Attack", false);
        }

        private void StandToAttack(bool isAttack)
        {
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                if (!isAttack) transform.rotation = new Quaternion(0.0f, transform.rotation.y, 0.0f, 0.0f);
                _animator.SetBool("Stance", isAttack);
            }
        }

        private void ThrowAttack()
        {
            if (Input.GetButtonDown("Fire2") && _isKnifeCoolDown)
            {
                var cameraRay = _mainCamera.ScreenPointToRay(Input.mousePosition);
                var groundPlane = new Plane(Vector3.up, Vector3.zero);
                if (groundPlane.Raycast(cameraRay, out _rayLegnth))
                {
                    var pointToThrow = cameraRay.GetPoint(_rayLegnth);
                    Debug.DrawRay(transform.position, pointToThrow, Color.green, _rayLegnth);
                    _gun.LookAt(pointToThrow);
                }
                Instantiate(_knife, _gun.position, _gun.rotation);
                KnifeCooling(_coolDown);
            }
        }

        private void ThrowBomb()
        {
            if (Input.GetButtonDown("Fire3") && _isBombCoolDown)
            {
                Instantiate(_bomb, _gun.position, _gun.rotation);
                BombCooling(_coolDown);
            }
        }

        private Transform Targeting(params GameObject[] mobs)
        {
            if (mobs.Length > 0)
            {
                var targetMob = mobs[0].transform;
                var directionToTargetMob = (transform.position - mobs[0].transform.position).sqrMagnitude;
                for (int i = 1; i < mobs.Length; i++)
                {
                    var nextTarget = mobs[i].transform;
                    var directionToNext = (transform.position - mobs[i].transform.position).sqrMagnitude;
                    if (directionToNext < directionToTargetMob)
                    {
                        targetMob = nextTarget;
                        directionToTargetMob = directionToNext;
                    }
                }
                return targetMob;
            }
            else return transform;
        }

        private void KnifeCooling(float time)
        {
            _isKnifeCoolDown = false;
            time -= Time.deltaTime;
            if (time >= 0) KnifeCooling(time);
            _isKnifeCoolDown = true;
        }

        private void BombCooling(float time)
        {
            _isBombCoolDown = false;
            time -= Time.deltaTime;
            if (time >= 0) BombCooling(time);
            _isBombCoolDown = true;
        }

        private void JumpCoolDown(float time)
        {
            time -= Time.deltaTime;
            if (time >= 0) JumpCoolDown(time);
            else _hasPlane = true;
        }

        #endregion
    }
}
