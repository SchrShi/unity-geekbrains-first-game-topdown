﻿using UnityEngine;
using UnityEngine.UI;


namespace Berserk.Player
{
    public class PlayerScoreCounter : MonoBehaviour
    {
        #region Fields
        [SerializeField] private Text _killsText;
        [SerializeField] private Text _keysText;
        [SerializeField] private EndGameManager _endGameManager;

        private int _killCount;
        private int _pickupKeysCount;

        private int _enemyCount = 0;
        private int _keysCount = 4;
        #endregion

        #region Properties
        public int KeysCount
        {
            get
            {
                return _pickupKeysCount;
            }
            private set
            {
                _pickupKeysCount = value;
            }
        }
        #endregion

        #region UnityMethods
        private void Start()
        {
            _enemyCount = GameObject.Find("Spawners").transform.childCount;
            _killCount = 0;
            _pickupKeysCount = 0;
            _killsText.text = $"Убито {_killCount} из  {_enemyCount}";
            _keysText.text = $"Книг собрано {_pickupKeysCount} из {_keysCount}";
        }
        #endregion

        #region Methods
        public void KillEnemy()
        {
            _killCount++;
            _killsText.text = $"Убито {_killCount} из  {_enemyCount}";
            if (_killCount >= _enemyCount) _endGameManager.EndGame(true);
        }

        public void PickupKey()
        {
            _pickupKeysCount++;
            _keysText.text = $"Книг собрано {_pickupKeysCount} из {_keysCount}";
        }
        #endregion
    }
}
