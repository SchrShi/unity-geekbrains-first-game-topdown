﻿using UnityEngine;
using Berserk.Player;


namespace Berserk.Items {
    public class HealController : Items
    {
        #region Fields
        [SerializeField] private float healthPoint = 25.0f;
        #endregion


        #region UnityMethods
        private void Update()
        {
                transform.Rotate(0.0f, Time.deltaTime * 3.0f, 0.0f);
        }
        #endregion


        #region Methods
        public override void Pickup(GameObject target)
        {
            if (target.CompareTag("Player"))
            {
                target.GetComponent<PlayerHealthManager>().Health(healthPoint);
                Destroy(gameObject);
            }
        }
        #endregion
    }
}
