﻿using UnityEngine;

namespace Berserk.Items
{
    public class Items : MonoBehaviour
    {
        #region UnityMethods

        private void OnCollisionStay(Collision other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                other.gameObject.GetComponent<Animator>().SetTrigger("isPickup");
                Pickup(other.gameObject);
            }

        }
        #endregion

        #region Methods
        public virtual void Pickup(GameObject target) { }
        #endregion
    }
}
