﻿using UnityEngine;
using Berserk.Player;


namespace Berserk.Items
{
    public class KeyController : Items
    {

        private void Update()
        {
                transform.Rotate(0.0f, Time.deltaTime * 3.0f, 0.0f);
        }


        public override void Pickup(GameObject target)
        {
            if (target.CompareTag("Player"))
            {
                target.GetComponent<PlayerScoreCounter>().PickupKey();
                Destroy(gameObject);
            }
        }
    }
}
