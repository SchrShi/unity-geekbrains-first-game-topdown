﻿using UnityEngine;


namespace Berserk.Items
{
    public class WeaponController : Items
    {
        #region Fields
        [SerializeField] private Transform _hand;
        [SerializeField] private float _damage;
        #endregion


        #region Properties
        public float Damage 
        { 
            get
            {
                return _damage;
            }
            private set
            {
                _damage = value;
            }
        }
        #endregion


        #region Methods
        public override void Pickup(GameObject target)
        {
            transform.parent = _hand;
            transform.rotation = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
            transform.position = _hand.transform.position;
            target.GetComponent<Animator>().SetBool("Sword", true);
            gameObject.GetComponent<Collider>().isTrigger = true;
        }
        #endregion
    }
}
